<!-- footer -->
<footer class="footer" role="contentinfo">
    <div class="logof">
        <a href="<?php echo home_url(); ?>">
            <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
            <img src="<?php echo the_field('logo_footer', 5) ?>" alt="Logo" class="logo-img">
        </a>
    </div>
    <!-- copyright -->
    <?php wp_nav_menu( array(
        'menu'           => 'menu_principal', // Do not fall back to first non-empty menu.
        'theme_location' => '__no_such_location',
        'fallback_cb'    => false // Do not fall back to wp_page_menu()
    ) ); ?>
    <!-- /copyright -->

</footer>
<!-- /footer -->


<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-80489136-1', 'auto');
    ga('send', 'pageview');

</script>

</div>
<!-- /wrapper -->

<?php wp_footer(); ?>



<script type="text/javascript">
    $(".toggle").click(function(e){
        $(".nav.mobile_menu .menu").toggle();
    });

    var link = window.location.href;
    var link2 = '<?php echo home_url()."/"; ?>';

    $(".nav a").click(function(e){
        e.preventDefault();
        var comprobar = $(this).attr("href");
        comprobar= comprobar.split("#");
        link = link.split("#");
        link = link[0];
        console.log(link);
        console.log(link2);
        if(comprobar[1]){
            if(link == link2){
                var target = this.hash;
                var $target = $(target);
                console.log(comprobar);
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top -100
                }, 900, 'swing', function () {
                    window.location.hash = target;
                });


                ga('send', 'pageview', target, 1);
                fbq('track', 'ViewContent');
            }else{
                var target = link2+'#'+comprobar[1];
                window.location.assign(target);
            }
        }else{
            var target = $(this).attr("href");
            window.location.assign(target);
        }
    });



    var words = ["LIVE","RIDE","RUN","CLIMB","CONQUER","EXPLORE","SHARE","ENJOY","LOVE","PLAY"];
    var index = 0;

    function preload(arrayOfImages) {
        $(arrayOfImages).each(function(){
            $('<img/>')[0].src = this;
            // Alternatively you could use:
            // (new Image()).src = this;
        });
    }


     $(document).ready(function(e){

        // Usage:
       /*  preload([
             '<?php echo get_template_directory_uri(); ?>/img/foto-1.png',
             '<?php echo get_template_directory_uri(); ?>/img/foto-2.png'

         ]);

        setInterval(function(e){
            index++;

            if(index == words.length){
                index = 0;

            }

            $("div.title h1").fadeOut("slow",function(e){
                $("div.title h1").html(words[index]);
                $("div.title h1").fadeIn("slow");

                if(index == 6){
                    $(".header").css("background","url(<?php echo get_template_directory_uri(); ?>/img/foto-2.png)");
                }

                if(index == 0){
                    $(".header").css("background","url(<?php echo get_template_directory_uri(); ?>/img/foto-1.png)");
                }

            });


        }, 3000);


        /*$(".articHare .buy_now").click(function(e){
            ga('send', 'pageview', "velo_arctic", 1);
            fbq('track', 'ViewContent');
            document.location.hash = '';
            $(".info").hide();
            $(".info_artic").show();
            $(".velo").fadeIn();
            document.location.hash = '!/~/product/id=68502604';

        });

        $(".articHare .see_more").click(function(e){
            ga('send', 'pageview', "velo_arctic", 1);
            fbq('track', 'Lead');

            document.location.hash = '';
            $(".info").hide();
            $(".info_artic").show();
            $(".velo").fadeIn();
            document.location.hash = '!/~/product/id=68502604';

        });

        $(".fennecFox .buy_now").click(function(e){
            ga('send', 'pageview', "velo_fennec", 1);
            fbq('track', 'Lead');

            document.location.hash = '';
            $(".info").hide();
            $(".info_fennec").show();
            $(".velo").fadeIn();
            document.location.hash = '!/~/product/id=68502602';

        });

        $(".fennecFox .see_more").click(function(e){
            ga('send', 'pageview', "velo_fennec", 1);
            fbq('track', 'Lead');

            document.location.hash = '';
            $(".info").hide();
            $(".info_fennec").show();
            $(".velo").fadeIn();
            document.location.hash = '!/~/product/id=68502602';

        });

        $(".btn_cerrar").click(function(e){
            ga('send', 'pageview', "cerrar_velo", 1);
            $(".velo").fadeOut();
        });*/

     });



</script>


</body>
</html>
