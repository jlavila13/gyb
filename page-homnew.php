<?php 
/**
*
*
* Template Name: Home New
*
*
*/

get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="features" id="features">
			<h1>Magic Features</h1>
			<?php
				query_posts('cat=3');
				if(have_posts()){
					while (have_posts()) {
						the_post();
						$imagen = wp_get_attachment_url( get_post_thumbnail_id() );
						$contenido = get_the_content();
						$titulo = get_the_title();
						echo "<li class='feature'>
								<img src='".$imagen."'>
								<h1>".$titulo."</h1>
								<p>".$contenido."</p>
							</li>";
					}
				}
			?>

		</section>
		<!-- /section -->
		<!-- section -->
		<section class="producto" id="product">

			<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id(11) ); ?>" alt="Logo" class="img_product">

		</section>
		<!-- /section -->
		<!--section-->
		<section class="whiteSpace" id="wspace">
			<?php
				query_posts('post_type=page&p=144');
				if(have_posts()){
					while (have_posts()) {
						the_post();
						$contenido = get_the_content();
						$titulo = get_the_title();
						echo "<li class='lspace'>
								".$contenido."
							</li>";
					}
				}
			?>
		</section>
		<!--/section-->
		<!-- section -->
		<section class="sponsor" style="background: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id(15) ); ?>');" id="media">
			<h1>As you have seen it on</h1>
			<?php
				query_posts('cat=4');
				if(have_posts()){
					while (have_posts()) {
						the_post();
						$imagen = wp_get_attachment_url( get_post_thumbnail_id() );
						$contenido = get_the_content();
						$titulo = get_the_title();


						echo "<li class='feature'>
								<a href='".$post->link."' target='_blank'> <img src='".$imagen."'></a>
							</li>";
					}
				}
			?>

		</section>
		<!-- /section -->
		<!-- section -->
		<section class="store" id="store">

			<div class="velo_buy">
                <div class="content">
			<?php
				query_posts('p=17&post_type=page');
				if(have_posts()){
					while (have_posts()) {
						the_post();
						$imagen = wp_get_attachment_url( get_post_thumbnail_id() );
						$contenido = get_the_content();
						$titulo = get_the_title();
						echo wpautop($contenido);
						echo do_shortcode('[email-subscribers namefield="NO" desc="" group="Public"]');
					}
				}
				

			?>
                    <button class='btn_cerrar'><img src="<?php echo get_template_directory_uri(); ?>/img/btn-cerrar.png"></button>
                </div>


			</div>
			<?php
				query_posts('cat=5');
				if(have_posts()){
					while (have_posts()) {
						the_post();
						$imagen = wp_get_attachment_url( get_post_thumbnail_id() );
						$contenido = get_the_content();
						$titulo = get_the_title();
						$precio = get_field("precio");
						echo "<li class='feature'>
								<img src='".$imagen."' class='imagen_thumb'>
								<div class='descripcion'>
									<h1>".$titulo."</h1>
									<h2>".$contenido."</h2>
									<p>".$precio."<span>$</span></p>
									<button id='velo'>buy</button>
								</div>
							</li>";
					}
				}
			?>

		</section>
		<!-- /section -->
	</main>

<script type="text/javascript">
	$("#velo").click(function(e){
		$(".velo_buy").slideDown();
        ga('send', 'pageview', "velo_compra", 1);
	})
	$(".btn_cerrar").click(function(e){
		$(".velo_buy").slideUp();
	})
</script>
<?php get_footer(); ?>
