<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>


    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon_1.ico" rel="shortcut icon">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta property="og:url"                content="http://justgotyourbag.com/" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="Dare to everything" />
    <meta property="og:description"        content="You are not a weakling. Dare today, we got your bag.   " />
    <meta property="og:image"              content="<?php echo get_template_directory_uri(); ?>/img/share.png" />
    <?php wp_head(); ?>
    <script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
    </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.12.4.min.js"></script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1788215081413537');
        fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1788215081413537&ev=PageView&noscript=1"
            /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body <?php body_class(); ?>>

<div id="wptime-plugin-preloader"></div>
<!-- wrapper -->
<div class="wrapper">

    <!-- header -->
    <header class="header clear" role="banner" style="background:url('<?php echo wp_get_attachment_url( get_post_thumbnail_id(5) ); ?>')">

        <!-- nav -->
        <nav class="nav" role="navigation" <?php if(is_home()){echo "style='position:fixed'";}else{echo "style='position:relative'";}?>>

            <!-- logo -->
            <div class="logo">
                <a href="<?php echo home_url(); ?>">
                    <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                    <img src="<?php echo the_field('logo', 5) ?>" alt="Logo" class="logo-img">
                </a>
            </div>
            <!-- /logo -->


            <?php wp_nav_menu( array(
                'menu'           => 'menu_principal', // Do not fall back to first non-empty menu.
                'theme_location' => '__no_such_location',
                'fallback_cb'    => false // Do not fall back to wp_page_menu()
            ) ); ?>
        </nav>


        <nav class="nav mobile_menu" role="navigation">

            <!-- logo -->
            <div class="logo">
                <a href="<?php echo home_url(); ?>">
                    <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                    <img src="<?php echo the_field('logo', 5) ?>" alt="Logo" class="logo-img">
                </a>
            </div>
            <!-- /logo -->


            <a href="javascript:void(0)" class="toggle"></a>
            <?php wp_nav_menu( array(
                'menu'           => 'menu_principal', // Do not fall back to first non-empty menu.
                'theme_location' => '__no_such_location',
                'fallback_cb'    => false // Do not fall back to wp_page_menu()
            ) ); ?>
        </nav>

        <!--clase borrada title-->
        <div class="sombra" <?php if(is_home()){echo "style='margin-top:100px'";}?>>
        <div class="row col-xs-12 titulo">
            <h1 class="titulo" style="line-height: 100px; text-align: left;margin-top:45px;">WE ARE MORE THAN A </br><span class="blanc">SOLAR BACKPACK</span></h1>
            <div class="h-icons" style="display:block; text-align: left;">
              <img style="display:inline-block" src="<?php echo get_template_directory_uri(); ?>/img/Panel-Solar.png" />
              <img style="display:inline-block" src="<?php echo get_template_directory_uri(); ?>/img/bateria.png" />
              <img style="display:inline-block" src="<?php echo get_template_directory_uri(); ?>/img/Agua.png" />
              <img style="display:inline-block" src="<?php echo get_template_directory_uri(); ?>/img/GoPro.png" />
            </div>
        </div>

        <div class="bolsos">
          <img src="<?php the_field('bolso_1', 5) ?>" alt="" style="display: inline-block;" class="">
          <img src="<?php the_field('bolso_2', 5) ?>" alt="" style="display: inline-block;" class="">
        </div>
        </div>
    </header>
    <!-- /header -->

    <?php if(!is_page_template('product-page.php')) {?>

    <div class="velo">
        <div class="content_velo">
            <a class="btn_cerrar" href="javascript:void(0)"></a>

            <div class="info info_artic">
                <img src="<?php echo get_template_directory_uri(); ?>/img/artic-hare-detail.png">
                <h5>Dare to <br>
                    Enjoy/share/love/play/live
                </h5>

                <div class="description">

                    <h2>THE BAG</h2>
                    <p>
                        Material: Nylon<br>
                        Size: 20x13x20 inches (52cm*35cm*22cm)<br>
                        Capacity : 30L<br>
                        Colors: Blue<br>
                    </p>


                    <h2>Cooler</h2>
                    <p>
                        Material: Nylon<br>
                        Size: 20x13x20 inches (52cm*35cm*22cm)<br>
                        Capacity : 30L<br>
                        Colors: Blue<br>
                    </p>

                    <h2>Solar Panel</h2>
                    <p>
                        Its made of Mono-crystalline sunpower solar cell 6.5W, 6V<br>
                        <span>Its removable tool, can be used with or without the bag.</span><br>
                        Efficiency: 22%<br>
                        Solar output: 5V,1.3A<br>

                    </p>

                    <h2>Portable Battery Charger</h2>
                    <p>
                        <span>Waterproof design.</span>
                        <span>Wireless switch.</span>
                        <span>Can charge two devices at the same time.</span>
                        Net weight: 230g
                        Battery: 10000 mAh.
                        Input: 5V,1.3A
                        Output: 5V,1.3A/ 5V, 2.1A (max)
                        Four LEDS display the capacity.
                        One USB charging cable.


                    </p>

                </div>
            </div>


            <div class="info info_fennec">
                <img src="<?php echo get_template_directory_uri(); ?>/img/fennec_detail.png">
                <h5>Dare to <br>
                    Ride/run/climb/conquer/explore/live
                </h5>

                <div class="description">
                    <h2>THE BAG</h2>
                    <p>
                        Material: Polyester<br>
                        Size: 6 x 1 x 6 inches (15.24cm*2.54*15.24cm)<br>
                        Capacity : 30-40L<br>
                        Colors: Brown<br>
                    </p>


                    <h2>Hydration pouch</h2>
                    <p>
                        Water bag capacity: 2L<br>
                        <span>Measure marks.</span>
                    </p>

                    <h2>Camera holder</h2>
                    <p>
                        <span>Are compatible with GoPro cameras and more.</span><br>
                    <span>It’s removable, so it can be used to put the cameras
                        at many outdoor places like edges, branches and high
                        rocks to take outstanding photos or breathtaking timelapses.
                    </span>

                    </p>

                    <h2>Solar Panel</h2>
                    <p>
                        Its made of Mono-crystalline sunpower solar cell 6.5W, 6V<br>
                        <span>Its removable tool, can be used with or without the bag.</span><br>
                        Efficiency: 22%<br>
                        Solar output: 5V,1.3A<br>

                    </p>

                    <h2>Portable Battery Charger</h2>
                    <p>
                        <span>Waterproof design.</span>
                        <span>Wireless switch.</span>
                        <span>Can charge two devices at the same time.</span>
                        Net weight: 230g
                        Battery: 10000 mAh.
                        Input: 5V,1.3A
                        Output: 5V,1.3A/ 5V, 2.1A (max)
                        Four LEDS display the capacity.
                        One USB charging cable.

                    </p>

                    <p>The portable battery charger, the camera holder and the solar panel are NOT SOLD SEPARATELY. </p>
                    <h2>Remember, we got your bag</h2>
                </div>
            </div>




        </div>
    </div>

    <? }?>
