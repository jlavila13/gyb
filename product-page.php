<?php
/**
 * Template Name: product
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>

    <section class="product">

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

            <div class="info_product">
                <img src="<?php echo get_field('logo_title'); ?>">
                <h5>Dare to <br> <?php echo get_field('texto_invitacional'); ?></h5>

                <div class="description">
                    <?php the_content();?>
                </div>
            </div>


            <div class="cart_product">

                <div id="my-store-9717056" style="padding-top: 140px"></div>
                <div>
                    <script type="text/javascript"
                            src="https://app.ecwid.com/script.js?9717056"
                            charset="utf-8">

                    </script>
                    <script type="text/javascript">
                        xProductBrowser("categoriesPerRow=3","views=grid(3,3) list(10) table(20)","categoryView=grid","searchView=list","id=my-store-9717056");
                        document.location.hash = '!/~/product/id=<?php echo get_field('product_id'); ?>';

                    </script>
                </div>
            </div>

        <?php endwhile; ?>

        <?php endif; ?>

    </section>
    <!-- /section -->

<script>


    /*$(window).on('hashchange', function() {
        $('html, body').animate({
            scrollTop: $('.product').offset().top - 140
        }, 'fast');
    });*/

    $(document).ready(function(){

        setTimeout(function(){
            $('html, body').animate({
                scrollTop: $('.product').offset().top
            }, 'fast');

        }, 5000);

    });
</script>
<?php get_footer(); ?>
