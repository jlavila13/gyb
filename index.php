<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="phrase1" id="phrase1">

            <h2>Listen to your animal spirit</h2>
            <h2>and dare to</h2>
        </section>
		<!-- /section -->

		<!-- section -->
		<section class="fennecFox" id="fennecFox">
            <h1>Ride/run/climb/conquer/explore/live</h1>

                <div>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/bolso.png">
                </div>

                <div>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/fennec-fox.png">
                    <h5>includes</h5>
                    <ul>
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/GoPro.png">
                        </li>

                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/Panel-Solar.png">
                        </li>

                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/Agua.png">
                        </li>

                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/bateria.png">
                        </li>
                    </ul>

                    <ul class="gopro">
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/logo_gopro.png">
                        </li>

                        <li>
                           <span>GoPro compatible</span>
                        </li>
                    </ul>

                    <span>*camera not inclued</span><br>
                    <!--a href="javascript:void(0)" class="see_more">+ see more</a-->
                    <a href="<?php echo get_home_url();?>/fennecfox" class="see_more">+ see more</a>

                </div>

                <div >
                    <!--a href="javascript:void(0)" class="buy_now">Buy now</a-->
                    <a href="<?php echo get_home_url();?>/fennecfox" class="buy_now">Buy now</a>
                </div>

		</section>
		<!-- /section -->



        <!-- section -->
        <section class="articHare" id="articHare">
            <h1>Enjoy/share/love/play/live</h1>
                <div>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/arctic-bag_site.png">
                </div>

                <div>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/artic-hare.png">
                    <h5>includes</h5>
                    <ul>

                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/Panel-Solar.png">
                        </li>

                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/cooler.png">
                        </li>

                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/img/bateria.png">
                        </li>
                    </ul>

<!--                    <a href="javascript:void(0)" class="see_more">+ see more</a>-->
                    <a href="<?php echo get_home_url();?>/arctichare" class="see_more">+ see more</a>

                </div>

                <div>
<!--                    <a href="javascript:void(0)" class="buy_now">Buy now</a>-->
                    <a href="<?php echo get_home_url();?>/arctichare" class="buy_now">Buy now</a>
                </div>

        </section>
        <!-- /section -->


        <section class="phrase2" id="phrase2">
            <h2>dare to <span>everything</span></h2>
        </section>


        <section class="phrase3" id="phrase3">
            <h2>“CUT OFF YOUR STATUS QUO BULL#%&T AND LIVE”</h2>
            <h2>-GOT YOUR BAG team.</h2>
        </section>


        <section class="phrase4" id="phrase4">
            <h2>“Praise the sun”</h2>
            <h2>-Solaire of Astora.</h2>
        </section>

		<!-- section -->
		<section class="share" id="share" >
				<h2>We dare you to share this page</h2>
				<a href="javascript:window.open('https://www.facebook.com/sharer/sharer.php?u=http%3A//justgotyourbag.com/','Facebook','width=600,height=400')" target="popup">
					<img src="<?php echo get_template_directory_uri(); ?>/img/btn-fb.png">
				</a>

        </section>
	</main>


<?php get_footer(); ?>
